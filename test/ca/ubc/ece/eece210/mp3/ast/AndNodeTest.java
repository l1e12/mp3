package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Genre;

public class AndNodeTest {

	@Test
	public void testInterpret(){
		AndNode and = new AndNode(new Token(TokenType.AND, "&&"));
		InNode in = new InNode(new Token(TokenType.IN, "in"));
		in.setArguments("Jazz");
		ByNode by = new ByNode(new Token(TokenType.BY, "by"));
		by.setArguments("Louis Armstrong");
		
		and.addChild(in);
		and.addChild(by);
		
		Album a = new Album("Louis and the Angels", "Louis Armstrong", new ArrayList<String>());
		Album b = new Album("Crossings", "Herbie Hancock", new ArrayList<String>());
		Album c = new Album("The Best of Chet Baker", "Chet Baker", new ArrayList<String>());
		
		Genre jazz = new Genre("Jazz");
		Genre rock = new Genre("Rock");
		jazz.addToGenre(a);
		jazz.addToGenre(b);
		Catalogue ca = new Catalogue();
		ca.add(jazz);
		ca.add(rock);
		
		Set<Element> albums = new HashSet<Element>();
		albums.add(a);
		
		assertEquals(and.interpret(ca), albums);
	}
	
}
