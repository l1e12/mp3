package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class CatalogueTest {

	static Catalogue c;
	static Album a1, a2, a3, a4;
	static Genre jazz, rock;
	static List<Album> albums;
	
	@BeforeClass
	public static void setup(){
		a1 = new Album("Louis and the Angels", "Louis Armstrong", new ArrayList<String>());
		a2 = new Album("Crossings", "Herbie Hancock", new ArrayList<String>());
		a3 = new Album("Don't Fear the Reaper", "Blue Oyster Cult", new ArrayList<String>());
		a4 = new Album("Ii", "Billy Talent", new ArrayList<String>());
		jazz = new Genre("Jazz");
		jazz.addToGenre(a1);
		jazz.addToGenre(a2);
		rock = new Genre("Rock");
		rock.addToGenre(a3);
		rock.addToGenre(a4);
		
		
		c = new Catalogue();
		
		c.add(jazz);
		c.add(rock);
		
		albums = new ArrayList<Album>();
	}
	
	
	@Test
	public void testQuery1(){ //root is OR, branches are &&
		albums.clear();
		albums.add(a3);
		albums.add(a1);
		
		assertEquals(albums, c.query("in (\"Jazz\") && matches (\".*Louis.*\") || (in (\"Rock\") && by (\"Blue Oyster Cult\"))"));
	}
	
	public void testQuery2(){
		albums.clear();
		albums.add(a4);
		albums.add(a3);
		
		assertEquals(c.query("by (\"Billy Talent\") || matches (\".*Reap.*\") && in (\"Rock\")"), albums);
	}
	
}
