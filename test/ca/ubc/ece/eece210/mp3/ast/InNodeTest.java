package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Genre;

public class InNodeTest {

	@Test
	public void testInterpret(){
		Token t = new Token(TokenType.IN, "in");
		InNode in = new InNode(t);
		in.setArguments("Jazz");
		
		Album a = new Album("Louis and the Angels", "Louis Armstrong", new ArrayList<String>());
		Album b = new Album("Crossings", "Herbie Hancock", new ArrayList<String>());
		Genre jazz = new Genre("Jazz");
		jazz.addToGenre(a);
		jazz.addToGenre(b);
		Catalogue c = new Catalogue();
		c.add(jazz);
		
		Set<Element> albums = new HashSet<Element>();
		albums.add(a);
		albums.add(b);
		
		assertEquals(in.interpret(c), albums);
	}
}
