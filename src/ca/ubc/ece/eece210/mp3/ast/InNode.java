package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class InNode extends ASTNode {

	public InNode(Token token) {
		super(token);
	}

	/**
	 * Finds all albums within a genre and all subgenres branching from the genre
	 * 
	 * @param argument  the catalogue to search through
	 * @return the set of albums in the genre
	 */
	@Override
	public Set<Element> interpret(Catalogue argument) {
		// TODO Auto-generated method stub
		Set<Element> genres = argument.getGenres();
		Set<Element> albums = new HashSet<Element>();
		Genre g = null;
		for(Element e : genres){
			if(((Genre) e).getTitle().equals(arguments)){
				g = ((Genre) e);
				break;
			}
		}
		
		if(g == null)
			throw new NullPointerException();
		
		for(Element _g : g.getSubGenres())
			albums.addAll(((Genre) _g).getAlbums());
        
		return albums;
	}

}
