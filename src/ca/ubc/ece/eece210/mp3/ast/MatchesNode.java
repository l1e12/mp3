package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class MatchesNode extends ASTNode {

    public MatchesNode(Token token) {
    	super(token);
    }

    /**
     * Finds all albums containing a given string "arguments"
     * 
     * @param argument  the catalogue to search through
     * @return the set of albums containing string "arguments"
     */
    @Override
    public Set<Element> interpret(Catalogue argument) {
	
		Set<Element> genres = argument.getGenres();
		Set<Element> albums = new HashSet<Element>();
		Set<Element> subAlbums = new HashSet<Element>();
		
		for(Element e : genres){
			albums.addAll(((Genre)e).getAlbums());
		}
		
		for(Element e : albums){
			if(((Album) e).getTitle().matches(arguments)){
				subAlbums.add(e);	
			}
		}
        
		return subAlbums;
	}

}