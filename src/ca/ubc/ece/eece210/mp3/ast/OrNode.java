package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

public class OrNode extends ASTNode {

    public OrNode(Token token) {
	super(token);
    }

    /**
     * Evaluates this node's child expressions
     * Combines the sets of albums produced by the children without duplicates
     * 
     * @param catalogue  the cataglogue to search through
     * @return the set of albums returned by the children of this node
     */
    @Override
    public Set<Element> interpret(Catalogue catalogue) {
	
    	Set<Element> albums = new HashSet<Element>();
    
		albums.addAll(children.get(0).interpret(catalogue));
		albums.addAll(children.get(1).interpret(catalogue));
		
    	return albums;
    
    }

}
