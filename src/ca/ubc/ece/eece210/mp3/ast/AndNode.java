package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 */

public class AndNode extends ASTNode {

	/**
	 * Create a new AndNode given a parser token
	 * 
	 * @param token
	 */
	public AndNode(Token token) {
		super(token);
	}

	/**
	 * Interpret the children of this node and creates the set of albums occurring in 
	 * both children's sets
	 * 
	 * @param catalogue  the catalogue in which to interpret the query
	 * @return set of albums satisfying the query
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		// TODO Auto-generated method stub
		Set<Element> albums = new HashSet<Element>();
		Set<Element> tempSet1 = new HashSet<Element>();
		Set<Element> tempSet2 = new HashSet<Element>();
	
		tempSet1 = children.get(0).interpret(catalogue);
		tempSet2 = children.get(1).interpret(catalogue);
		
		for(Element e: tempSet1){
			if(tempSet2.contains(e))
				albums.add(e);
	 }
	return albums;

   }
}
