package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Genre;

public class ByNode extends ASTNode{

	public ByNode(Token token) {
		super(token);
	}

	/**
	 * Finds all albums in catalogue by a certain performer
	 * 
	 * @param catalogue  the catalogue in which to interpret the query
	 * @return set of albums by the node's "arguments"
	 */
	@Override
	public Set<Element> interpret(Catalogue argument) {
		
		Set<Element> genres = argument.getGenres();
		Set<Element> albums = new HashSet<Element>();
		Set<Element> subAlbums = new HashSet<Element>();
		
		for(Element e : genres){
			albums.addAll(((Genre)e).getAlbums());
		}
		
		for(Element e : albums){//Used equals method. Should work.
			if(((Album) e).getPerformer().equals(arguments)){
				subAlbums.add(e);	
			}
		}
        
		return subAlbums;
	}



	}

