package ca.ubc.ece.eece210.mp3;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.ast.ASTNode;
import ca.ubc.ece.eece210.mp3.ast.OrNode;
import ca.ubc.ece.eece210.mp3.ast.QueryParser;
import ca.ubc.ece.eece210.mp3.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp3.ast.Token;

/**
 * Container class for all the albums and genres. Its main responsibility is to
 * save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {

	private List<Element> contents;

	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		contents = new ArrayList<Element>();
	}

	public int size() {
		return contents.size();
	}
	
	public Element get(int index) {
		return contents.get(index);
	}
	
	public void add(Element e) {
		contents.add(e);
	}

	/**
	 * Builds a new catalogue and restores its contents from the given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		// TODO implement
		// HINT:  look at Genre.restoreCollection(...)
		
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * 
	 * @param fileName
	 *            the file where to save the library
	 */
	public void saveCatalogueToFile(String fileName) {
		// TODO implement
	}
	
	public Set<Element> getGenres(){
		Set<Element> genres = new HashSet<Element>();
		for(Element e: contents){
			if(e.hasChildren())
				genres.addAll(((Genre) e).getSubGenres());
		}
		return genres;
	}
	/**
	 * Takes a query and returns the ablums in the catalogue satisfying 
	 * the query
	 * 
	 * @param queryString
	 * @return List of albums satisfying the query
	 */
	public List<Album> query(String queryString) { 
		List<Token> tokens = QueryTokenizer.tokenizeInput(queryString);//get tokens
		QueryParser parser = new QueryParser(tokens);//parse said tokens
		Set<Element> element = new HashSet<Element>();//ooo a list :O
		List<Album> albums = new ArrayList<Album>();
		
		ASTNode root = parser.getRoot();
		element.addAll(root.interpret(this));
		
		for(Element e: element){
			albums.add((Album) e);
		}
		
		return  albums;
	}
}
